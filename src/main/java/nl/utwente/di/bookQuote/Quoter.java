package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> bookPrices;

    public Quoter() {
        bookPrices = new HashMap<>();
        bookPrices.put("1", 10D);
        bookPrices.put("2", 45D);
        bookPrices.put("3", 20D);
        bookPrices.put("4", 30D);
        bookPrices.put("5", 50D);
    }

    public double getBookPrice(String book) {
        return bookPrices.getOrDefault(book, 0D);
    }
}
